# Picture to Pattern With Inkscape 


Import the image to inkscape.
![](../images/tracing/import_img.png)

Open the menu view layers.
![](../images/tracing/view_layers.png)

Lock the imported image.
![](../images/tracing/lock_layer1.png)

Add layer
![](../images/tracing/add_layer.png)

Grab the beier tool
![](../images/tracing/bezier.png)

Start to trace the image and get the nodes.
![](../images/tracing/tracing.png)

To get the curves, you could edit the paths or adjust the nodes by moving it.
![](../images/tracing/edit_paths.gif)

Hide the image
![](../images/tracing/hide_layer.gif)

Change the stroke width
![](../images/tracing/stroke_width.png)

Export the file into dxf format
![](../images/tracing/dxf.png)


![](../images/tracing/fish_tail.png)

## Useful links

- [Inkscape](https://inkscape.org/)

## Reference

- https://www.britannica.com/animal/porgy-fish

