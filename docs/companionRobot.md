# Companion Robot
![](../images/companionRobot/graduation.jpeg)

![](../images/companionRobot/model.png)

## Demonstration
![](../images/companionRobot/demo.gif)

## 3D CAD
![](../images/companionRobot/3DCAD.png)
![](../images/companionRobot/3DCAD1.png)

## Laser Cut

Material : 3mm plywood

![](../images/companionRobot/2D.png)

## Pictures

### Biyun02 Spring Class
![](../images/companionRobot/assembly.JPG)

![](../images/companionRobot/assembly2.JPG)


![](../images/companionRobot/laserCut.JPG)

![](../images/companionRobot/inkscape.PNG)

### Summer Camp 2020
![](../images/companionRobot/activity.jpeg)

![](../images/companionRobot/activity1.jpeg)

## Part List
- 3mm plywood
- Mixly box
- 5 x AA Batteries
- 2 x wheel
- 3 MM screws and nuts
- IR sensor
- Switch



## Software
- Inkscape
- Arduino IDE

## Downloads

<a href="../images/companionRobot/laserCut_roboticToy.DXF" download>Laser Cut Template</a>

<a href="../images/companionRobot/companion robot 1.0.dwg" download>TianQi - Updated Laser Cut Template</a>

## Useful Links
- [Mixly Tutorial](http://fab.pages.fablabo.org/2020/wiki/tutorials/mixly/)