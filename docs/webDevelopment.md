---
layout: page
title: Web Development
permalink: /WebDevelopment
---

# Introduction to front end development

This week we are going to be a front end developer and start to get used to the documentation process using git and html.

We will talk about what are HTML, CSS and Javascript. How they fit togther and what roles they play. Are they similiar or how are they different? We are going to really just dissect what they do, so we wil have a little bit better idea of how it fits with all the other pieces.

## Objectives

-   Setup Developer Environment
-   Define roles of HTML, CSS and Javascript
-   Write a proper structured HTML document
-   Write a common closing and self closing tags
-   Recreate a simple website based on a provided photo

## Setting up developer environment

You need to install the following software

-   [Chrome](https://www.google.com/chrome/)
-   [Bracket](http://brackets.io/)

We are going to learn about two tools that we are going to use for the rest of this course. When we make a website, when we are developing web applications we need a place to write our code which should be a text editor and we need a place to view our code which would be a web browser.

We use our web browser all the time. It is how you view the internet. You might using your favourite web browser or the one that come with your machine. It might be Safari or chrome or Firefox or Internet Explorer.

There is a bunch of web browsers out here and they all do basically the same thing. They are all let you do the same thing. However, we are going to use Google Chrome in this course for two main reasons. One is that we just want to have a standardized browser so that if I do something I show a shortcut. I show the developer tools I do something one of the menus that you can replicate it yourself. And the other reason is that Google Chrome is used by a lot of developers. So as I mentioned you can use any of the browsers but generally most developers are using Google Chrome or Mozilla Firefox. Both of them are solid offerings especially with their developer tools which we are going to learn a lot more about this as we move forward.

Basically dev tools are tools that are made for developers, so things that you may have never seen before because you have never been a developer. But as soon as you start, we are going to be using it a lot of the tools that chrome gives us. If you have not been familiar, use Chrome to surf on the internet in daily. There is no sense to use Safari to browse and develop your website on Chrome.

The other tools that we need is a text editor.
This is a really important tool, it is where we are going to write our code. Technically we could use any text editor out there so you could write code in Microsoft Word. You could do it in a Google doc. You could do it a five hundred dollar professional code editor. Honestly every developer has his or her own opinions and his or her own set up. Personally I prefer teaching with a free text editor. 

## Introduction to the web


The first that I want to highlight there, the user in this case went to a website and then hit enter and all of this is happening before the user see anything.
The idea is that a user is asking for a website or requesting it. So when I opened up a website, you know when I hit “Enter”. I am asking for the contents of this page and that has to go all the way to somewhere and they have to send it all the way back to me. So far we are just getting the request to the right place.

After the video has done.
Give an example by opening a website.
Remember when I hit “enter”. I am asking for something and I am getting something back and requesting something and the server is responding. There are a lot of stuffs in between but that is the core concept.

We can view the underlying HTML, CSS and Javascript on any webpage.
In chrome, right click and select “view page source”.
So the browser is a complicated thing but the good thing is, you do not have to know how that works. That is the whole work of the browser. It takes that code and turns it into this.


## The Front End

![](./assets/images/web/frontDev.png)

-   The Front End is the Stuff that you see and interact with HTML, CSS and JS.
-   The Back End is everything else: so many choices!
-   Restaurant Analogy: The backend is everything that happens in the kitchen; the front end is what is plated and sent to your table.

##  HTML
-   Hyper Text Markup Language
-   Defines the structure of a webpage
    -   Put an image here
    -   Put a form here
-   The “nouns” of a webpage

## CSS
-   Cascading Style Sheets
-   Define the style of HTML
    -   Make all text green
    -   Give the first image a yellow border
    -   The “adjectives” of a webpage

## JavaScript
-   Adds Logic and interactivity to a page
    -   Do some math
    -   Change colour when the user clicks
-   The action or “verbs” of a webpage

## The general rule - HTML

```
<tagName>Some Content</tagName>
```

[HTML fundamentals](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/HTML_text_fundamentals
)

### Comments
```
<!-- This is a comment. It does not do anything! -->
```

To comment: you can type it or press ctrl + /.
We should have nothing floating outside the html. 


## Boilerplate - "Starter Kit"

![](./assets/images/web/boilerPlate.png)

```
<!DOCTYPE html>
<html>
<head>

<!-- metadata goes in head -->

	<title>My First Page</title>
</head>
<body>
<!-- content goes in the body -->
	<h1>This is my first tag!!!</h1>
</body>
</html>
```

## List
```
<!DOCTYPE html>
<html>
<head>
	<title>Things I've Learned</title>
</head>
<body>

<h1>Things I've Learned</h1>


<ol>
	<li>Setup Developer Environment</li>
	<li>Define roles of HTML, CSS and Java script</li>
	<li>Write properly structured HTML documents</li>
    <li>Write common closing and self closing tags</li>
</ol>

<h2>HTML</h2>

<ul>
	<li>Stands for <strong>Hyper Text Markup Language</strong></li>
	<li>Lots of Tags
	 <ul>
	 	<li>Boilerplate
	 	  <ol>
	 	  	<li>Doctype</li>
	 	  	<li>HTML</li>
	 	  	<li>Head
	 	  		<ol>
	 	  			<li>Title</li>
	 	  		</ol>
	 	  	</li>
	 	  	<li>Body</li>
	 	  </ol>
	 	</li>
	 	<li>Headings</li>
	 	<li>Paragraph</li>
	 	<li><strong>strong</strong></li>
	 </ul>
	</li>
</ul>
    
</body>

</html>
```


## Atributes
```
<tag name="value"></tag>
<img scr="picture.png">
<a href=www.fablabo.com>Click me to go to Fablab O website>
<link rel="stylesheet" type="text/css" href="style.css">
```

## Images
```
<img src="image.png">
```
<img scr="image.png">

## Basic Tags
### Closing Tags
```
<h1>I need a closing tag</h1>
<p>Me too</p>
```

### Self Closing Tags
```
<!--    No closing tag or inner text needed -->
<img scr="image.png">
<link rel="stylesheet" type="text/css" href="style.css">
<!--    Do not worry about what these tags do yet   -->
```

## CSS- General Rule

```
Selector    {
    property: value;
    another property: value;
    
}
```

CSS examples:
```
h1 {
	/*Named Colors*/
	color: blue;
}

li {
	/*HEXADECIMAL*/
	color: #F400DD;
}

h4 {
	/*RGB*/
	color: rgb(240, 100, 87);
}

```

-   [Colour Picker](https://www.webfx.com/web-design/color-picker/)
-   [Codepen.io](http://codepen.io/Colt/pen/WQQVvE)

# HTML & CSS Website Template
## Simple template
This is a simple website template made only with HTML & CSS.

You could just start using the following tags and insert the document your works.

## HTML
```
<!-- 
Marcello Tania 10/04/2020
(c) Fablab O 2020

This work may be reproduced, modified, distributed,
performed, and displayed for any purpose. Copyright is
retained and must be preserved. The work is provided
as is; no warranty is provided, and users accept all 
liability.
-->
<!DOCTYPE html>
<html>
    <head>
    <title>Name</title>
    <link href="style.css" rel="stylesheet">
    </head>
    
    <body>
        <!-- Static navbar -->
    <div class="header">
      <a href="https://fablabo.org/" class="logo">Fablab O</a>
      <div class="header-right">
        <a class="active" href="#home">Home</a>
          
        <a href="finalProject.html">Final Project</a>
      </div>
    </div>
        
    <div class="content">
        
        <!-- Insert your content here below! -->

        <!-- End of your content -->

    </div>
        
    <footer id="footer">
        <p class="footer">&copy; Copyright 2020</p>
    </footer>
    </body>
    
</html>

```

## CSS
```
/*--------------------
Import Google Fonts
Marcello Tania
--------------------*/

/* main font for menu. */
@import url(http://fonts.googleapis.com/css?family=Roboto:400|Raleway:200,300,400,500,700,900);

/*--------------------
1. General
--------------------*/

body {
	margin: 0px auto;
	font-family: "Raleway", Sans-serif;
    min-width: 80%;
    margin: 0px auto;
}

h2,
h1{
	font-weight: 200;
	letter-spacing: 0.4px;
	color: #000000;
    font-weight: 500;
}
h3{
    font-size: 20px;
    
}
p,
a,
ul{
	font-size: 20px;
	font-weight: 300;
	line-height: 28px;
	letter-spacing: 0.2px;
	color: black;
}

.content{
    margin: 0 10%;
    min-height: calc(100vh - 70px);
}

.imgCenter {
  display: block;
  margin-left: auto;
  margin-right: auto;
  max-width:70%;
}



/*-----------------------

2. Header

--------------------*/

/* Style the header with a grey background and some padding */
.header {
  overflow: hidden;
  background-color: #f1f1f1;
  padding: 20px 8%;
  margin: auto;
}

/* Style the header links */
.header a {
  float: left;
  color: black;
  text-align: center;
  font-size: 18px;
  padding: 12px;
  text-decoration: none;
  line-height: 25px;
  border-radius: 4px;
}

/* Style the logo link (notice that we set the same value of line-height and font-size to prevent the header to increase when the font gets bigger */
.header a.logo {
  font-size: 35px;
  font-weight: bold;
}

/* Change the background color on mouse-over */
.header a:hover {
  background-color: #ddd;
  color: black;
}

/* Style the active/current link*/
.header a.active {
  background-color: dodgerblue;
  color: white;
}

/* Float the link section to the right */
.header-right {
  float: right;
}

/* Add media queries for responsiveness - when the screen is 500px wide or less, stack the links on top of each other */
@media screen and (max-width: 500px) {
  .header a {
    float: none;
    display: block;
    text-align: left;
  }
  .header-right {
    float: none;
  }
}




/* The dropdown container */
.dropdown {
  float: left;
  overflow: hidden;
}

/* Dropdown button */
.dropdown .dropbtn {
  font-size: 18px;
  border: none;
  outline: none;
  color: black;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit; /* Important for vertical align on mobile phones */
  margin: 0; /* Important for vertical align on mobile phones */
}

/* Dropdown content (hidden by default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

/* Add a grey background color to dropdown links on hover */
.dropdown-content a:hover {
  background-color: #ddd;
}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
  display: block;
}


/* Code box */
pre {
    display: block;
    padding: 30px;
    margin: 0 0 10px;
    font-size: 15px;
    line-height: 1.42857143;
    color: #333;
    word-break: break-all;
    word-wrap: break-word;
    background-color: #f5f5f5;
    border: 1px solid #ccc;
    border-radius: 4px;
}

/* Footer */
#footer {
    bottom: 0;
    background-color: #eee;
    padding: 0px 100px;
    height: 30px;
}

.footer {
    font-size: 15px;
    text-align: center;
}
```


### Basic Tags
```
<h1>Title</h1>
<h2>Subtitle</h2>
<p>Paragraph</p>
```
### Image
```
<img src="" class="imgCenter">
```
### Presentation Viewer
```
<iframe src="media/.pdf" width="100%" height="800px"></iframe>
```
- http://root.pages.fablabo.org/html-template

### Download Link
```
<a href="" download="">Download</a>
```

### Download
- https://fabocloud.cn/root/html-template

## Useful Links
-   [developer Mozilla](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/HTML_text_fundamentals
)
-   [FabAcademy 2015- HTML&CSS](http://www.fabacademy.org/archives/2015/doc/html-css.html
)
-   [Element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)


</div>
