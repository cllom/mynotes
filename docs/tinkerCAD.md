---
layout: page
title: TinkerCAD
permalink: /TinkerCAD
---


# TinkerCAD

![](./assets/images/tinkerCAD/pinguins.png)


## Objectives

-   Understand the interface and how to navigate 
-   How to select an object? 
-   How to move and object (rotate, move) 
-   How to copy and delete an object 
-   How to resize an object? 
-   Group, ungroup, align and mirror 
-   How to change the object color? 

## Direct starters
### Place it!
![](./assets/images/tinkerCAD/placeIt.gif)
### View it!
Right click - to rotate
Middle click - to pan
Scroll - to zoom in and out
![](./assets/images/tinkerCAD/viewIt.gif)
### Move it!
![](./assets/images/tinkerCAD/moveIt.gif)
### Rotate it!
![](./images/tinkerCAD/rotateIt.gif)
### Size it up!
![](./assets/images/tinkerCAD/sizeIt.gif)
### Group it !
![](./assets/images/tinkerCAD/groupIt.gif)


# Pinguin Tutorial

Place a cylinder
![](./assets/images/tinkerCAD/cylinder.png)

Resize the cylinder and use the ruler relative to the bottom left corner as a reference.
![](./assets/images/tinkerCAD/resize.png)

Make another cylinder to make a hollow cylinder
![](./assets/images/tinkerCAD/group.png)

![](./images/tinkerCAD/hollow.png)
Create a 50x50x50 sphere
![](./assets/images/tinkerCAD/sphere.png)

Cut the sphere into half
![](./assets/images/tinkerCAD/halfSphere.png)

Make the half sphere and place it at the centre of the other half sphere using the ruler.
Change the shape into "hole" and group them togehter to build a hollow half sphere
![](./assets/images/tinkerCAD/hollowSphere.png)

Move the half sphere on the top of hollow cylinder
![](./assets/images/tinkerCAD/move.png)

Group both objects and change the colour. Now create two cylinder and create the eyes like the picture bellow.
![](./assets/images/tinkerCAD/eyes.png)

Create the hands and feel free to decorate them.
![](./assets/images/tinkerCAD/hand.png)

Here is my pinguin!
![](./assets/images/tinkerCAD/pinguin.png)

It's time to slice it and print it with a 3D printer.
![](./assets/images/tinkerCAD/slicer.png)

## Downloads

<a href="../images/tinkerCAD/Pinguin.stl" download>Pinguin STL</a>

## Useful Links
-   [TinkerCAD](https://www.tinkercad.com/)


</div>
