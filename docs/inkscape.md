# Inkscape

## Basic
### Path
![](../images/inkscape/path.png)

## Guidlines how to design a doughnut logo

The first thing you want to set is the view to custom and the display to 1:1.
![](../images/inkscape/view_custom.png)

![](../images/inkscape/zoom.png)

Open the fill and stroke menu.
![](../images/inkscape/fill_stroke.png)

Open the align and distribute objects menu.
![](../images/inkscape/align.png)

![](../images/inkscape/relative_last_selected.png)

Create a circle.
![](../images/inkscape/create_circle.png)

Hold shift+ctrl on the keyboard and click.
Drag it into the canvas to make a perfectly round circle.
And then go back to the select tool.

![](../images/inkscape/select_tool.png)



Click the locked button and make the width 500 px
![](../images/inkscape/locked.png)

Drop the opacity so we could see through the circle.
![](../images/inkscape/opacity.png)

Right click and duplicate.
![](../images/inkscape/duplicate.png)

Resize the duplicated circle into 175 px.
![](../images/inkscape/resize.png)

Center on vertical axis with realative to the last selected.
![](../images/inkscape/align_center.png)

Center on horizontal axis.
![](../images/inkscape/center_horizontal.png)

Select the two circles and open the path menu, select difference.
![](../images/inkscape/difference.png)
Right click and duplicate.
Turn the colour into red.
Resize the duplicated circle little bit larger by hitting Ctrl+0 four times.

![](../images/inkscape/circle.png)

Create a rectangle going over half of the doughnut.

![](../images/inkscape/rectangle.png)

Select the red hollow circle and the rectangle and then select path, click difference.

![](../images/inkscape/path_difference.png)

Edit path by notes
![](../images/inkscape/edit_path.png)

Click and drag between the nodes.
![](../images/inkscape/drag.png)

Select the handle and shift it, so it's parallel with this handle.
Bring it down a bit.
Repeat it on the other side of the node.
![](../images/inkscape/detail_drag.png)

Select two nodes and then hit insert new nodes.
![](../images/inkscape/insert_new_nodes.png)

Click and drag up the middle node.
Adjust the handle, so it's horizontally align by holding ctrl.

![](../images/inkscape/nodes_horizontal.png)

Do the same thing over the other side.
![](../images/inkscape/other_side.png)

Colour
![](../images/inkscape/colour.png)

Opacity
![](../images/inkscape/opacity_100.png)

Duplicate and set the colour into black
Lower selection one step (page down)
![](../images/inkscape/lower_selection.png)

Select the black toping and int the stroke pain select flat color.
![](../images/inkscape/flat_color.png)

In the stroke style select edit the width 25 px, select rounded join and rounded cap.

![](../images/inkscape/stroke_style.png)

Select Path and stroke to path.
![](../images/inkscape/stroke_path.png)

Select Path and break apart.
Select Path and union.

![](../images/inkscape/break_apart.png)

Press F7 on the keyboard to get our dropper tool and click on the brown segment

![](../images/inkscape/f7.png)

f1

Path inset or ctrl+9 in windows or command+9 few times.

![](../images/inkscape/path_inset.png)

Duplicate the white part.

Duplicate the brown top layer
![](../images/inkscape/duplicate_brown.png)

Path and select intersection
![](../images/inkscape/intersection.png)

Create a circle without a stroke
![](../images/inkscape/circle_bite.png)
Duplicate the circle and shift it by holding ctrl.

Select all of them and on path select union
![](../images/inkscape/union.png)

Duplicate three times and group them
![](../images/inkscape/group.png)

Rotate it by clicking the same object second times.
![](../images/inkscape/rotate.png)

Ungroup them
![](../images/inkscape/ungroup.png)

Select the bites and the toping, then select path difference.
![](../images/inkscape/difference_path.png)

Repeat the process for all layers
![](../images/inkscape/doughnut_bite.png)

Create a rounded rectangle by having a fillet
![](../images/inkscape/rounded_rectangle.png)

Duplicate the rectangle and shift it all over the topings.
Change the colours.
![](../images/inkscape/doughnut.png)

## Useful links

- [Dounut Tutorial](https://www.youtube.com/watch?v=s41_-qsmA7s)
- [Inkscape](https://inkscape.org/)
- http://keyboardchecker.com/


