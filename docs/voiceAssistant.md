# Voice Assistant

# Test Your Imports
```
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
import speech_recognition as sr
```

# Quick Start Guide

```
# Create a new chat bot
from chatterbot import ChatBot
# The only required parameter for the ChatBot is a name. This can be anything you want.
chatbot = ChatBot("Ron Obvious")
```

```
## Training your ChatBot

from chatterbot.trainers import ListTrainer

conversation = [
    "Hello",
    "Hi there!",
    "How are you doing?",
    "I'm doing great.",
    "That is good to hear",
    "Thank you.",
    "You're welcome."
]

trainer = ListTrainer(chatbot)

trainer.train(conversation)
```

```
## Get a response

response = chatbot.get_response("How are you doing?")
print(response)
```

## Chatbot

```
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer

import speech_recognition as sr

from gtts import gTTS
import os

# Create a new chat bot named 'INSERT NAME'
chatbot = ChatBot("Paula")
chatbot.storage.drop()

conversation = [
    "Hello",
    "Hi there!",
    "How are you doing?",
    "I'm doing great.",
    "That is good to hear",
    "Thank you.",
    "You're welcome."
]

trainer = ListTrainer(chatbot)

trainer.train(conversation)
# obtain audio from the microphone
r = sr.Recognizer()



tts = gTTS(text="Welcome to Fab O assistant, how can I help you? I am Marcello assistance.", lang='en')
tts.save("good.mp3")
os.system("afplay good.mp3")

#main loop
while True:
	#Stores the voice command
	command = ""
	
	
	with sr.Microphone() as source:
		print("\n\n\nSay something!")
		audio = r.listen(source)

    	# recognize speech using Google Speech Recognition
	try:
		# for testing purposes, we're just using the default API key
		# to use another API key, use 'r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")'
		# instead of 'r.recognize_google(audio)'
		command = r.recognize_google(audio)
		print(command)
        
	except sr.UnknownValueError:
		print("Speech Recognition could not understand audio")
		continue
	except sr.RequestError as e:
		print("Could not request results from Speech Recognition service; {0}".format(e))
		continue
	
	response = str(chatbot.get_response(command))
    
    
    #Print and say the response
	print("response: " + response)
    
	if response != "":
		tts = gTTS(text=response, lang='en')
		tts.save("good.mp3")
		os.system("afplay good.mp3")
    
```

## Shut Down Feature
```
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
## add a text to speech
import speech_recognition as sr

from gtts import gTTS
import os

# Create a new chat bot named 'INSERT NAME'
chatbot = ChatBot("Paula")
chatbot.storage.drop()

conversation = [
    "Hello",
    "Hi there!",
    "How are you doing?",
    "I'm doing great.",
    "How are you ",
    "I'm good.",
    "What are you doing",
    "I am helping Marcello to build myself and preparing some material courses. That's all! Thank you.",
    "What is your name",
    "I don't have the name yet. Could you please suggest me a name.",
    "Who are you",
    "I am Marcello's assistant.",
    "What are you up to",
    "I've been helping you work",
    "What is your favorite food",
    "DC voltage with a side of current",
    "Are you there",
    "I am here",
    "Can you hear me",
    "I can hear you",
    "bye",
    "Goodbye"
]

trainer = ListTrainer(chatbot)

trainer.train(conversation)
# obtain audio from the microphone
r = sr.Recognizer()



tts = gTTS(text="Welcome to Fab O assistant, how can I help you?", lang='en')
tts.save("good.mp3")
os.system("afplay good.mp3")

#main loop
done = False
while done == False:
	#Stores the voice command
	command = ""
	
	
	with sr.Microphone() as source:
		print("\n\n\nSay something!")
		audio = r.listen(source)

    	# recognize speech using Google Speech Recognition
	try:
		# for testing purposes, we're just using the default API key
		# to use another API key, use 'r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")'
		# instead of 'r.recognize_google(audio)'
		command = r.recognize_google(audio)
		print(command)
        
	except sr.UnknownValueError:
		print("Speech Recognition could not understand audio")
		continue
	except sr.RequestError as e:
		print("Could not request results from Speech Recognition service; {0}".format(e))
		continue
        
    	#Print response
	if "shut down" in command.lower():
		response = "Shutting down! Good bye! Thank you! This is Fab O X!"
		done = True
	else:
		response = str(chatbot.get_response(command))
    
    
    #Print and say the response
	print("response: " + response)
    
	if response != "":
		tts = gTTS(text=response, lang='en')
		tts.save("good.mp3")
		os.system("afplay good.mp3")

```
## Useful Links
- [chatterbot](https://pypi.org/project/ChatterBot/)