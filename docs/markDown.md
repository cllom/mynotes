---
layout: page
title: Markdown
permalink: /Markdown
---


# Markdown
This is intended as a quick reference and showcase. For more complete info, see [GitHub Pages](https://help.github.com/en/github/writing-on-github/basic-writing-and-formatting-syntax#links).

- Headers
- Lists
- Links
- Images
- Code



# Headings
```
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:
Alt-H1
======

Alt-H2
------

```
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:
Alt-H1
======

Alt-H2
------

# Lists
```
1. First ordered list item
2. Another item
⋅⋅* Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
⋅⋅1. Ordered sub-list
4. And another item.

⋅⋅⋅You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

⋅⋅⋅To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
⋅⋅⋅Note that this line is separate, but within the same paragraph.⋅⋅
⋅⋅⋅(This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses

```

1. First ordered list item
2. Another item
⋅⋅* Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
⋅⋅1. Ordered sub-list
4. And another item.

⋅⋅⋅You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

⋅⋅⋅To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
⋅⋅⋅Note that this line is separate, but within the same paragraph.⋅⋅
⋅⋅⋅(This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses


# Links

There are two ways to create links.

```
[go to google](https://www.google.com)


```

[go to google](https://www.google.com)



# Image

```
![](./assets/images/markDown/markDown.png)
```


![](./assets/images/markDown/markDown.png)


# Code

Inline `code` has `back-ticks around` it.


```
Inline `code` has `back-ticks around` it.

```

Blocks of code are either fenced by lines with three back-ticks <code>```</code>


# Markdown CI/CD Configuration

```
image: python:alpine

before_script:
  - pip install MarkupSafe --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/MarkupSafe-1.1.1.tar.gz?inline=false
  - pip install PyYAML --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/PyYAML-5.3.tar.gz?inline=false
  - pip install Jinja2 --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/Jinja2-2.11.1-py2.py3-none-any.whl?inline=false
  - pip install click --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/Click-7.0-py2.py3-none-any.whl?inline=false
  - pip install tornado --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/tornado-6.0.4.tar.gz?inline=false
  - pip install six --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/six-1.14.0-py2.py3-none-any.whl?inline=false
  - pip install livereload --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/livereload-2.6.1-py2.py3-none-any.whl?inline=false
  - pip install future --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/future-0.18.2.tar.gz?inline=false
  - pip install nltk --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/nltk-3.4.5.zip?inline=false
  - pip install lunr[languages]==0.5.6 --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/lunr-0.5.6-py2.py3-none-any.whl?inline=false
  - pip install Markdown --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/Markdown-3.2.1-py2.py3-none-any.whl?inline=false
  - pip install pymdown-extensions --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/pymdown_extensions-6.3-py2.py3-none-any.whl?inline=false
  - pip install Pygments --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/Pygments-2.5.2-py2.py3-none-any.whl?inline=false
  - pip install mkdocs --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/mkdocs-1.1-py2.py3-none-any.whl?inline=false
  - pip install mkdocs-material --no-index --find-links https://fabocloud.cn/root/mkdocs-1.1/-/raw/master/mkdocs_material-4.6.3-py2.py3-none-any.whl?inline=false

pages:
  script:
  - mkdocs build
  - mv _site public
  artifacts:
    paths:
    - public
  only:
  - master


```

# reference
- [stackedit](https://stackedit.io/app#)
- [adam-p](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#lists)

