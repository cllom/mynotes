# 2D Design with TinkerCAD

## Technical Drawing
![](../images/dvdCNC/arduinoMounter.PNG)
This technical drawing helps you to know the dimension of the object that we would like to draw.

## Tutorial

![](../images/2dTinkerCAD/place.png)
Place a box and resize the dimension like the picture above.

![](../images/2dTinkerCAD/hole.png)
Place a cylinder hole and resize the dimension and locate it correctly given from the technical drawing.

![](../images/2dTinkerCAD/pasteHole.png)
Copy and paste the cylinder hole and relocate it according to the the technical drawing.

![](../images/2dTinkerCAD/arduinoHole.png)
Create another cylinder hole with 3.2 mm in diameter and locate it using the ruler.
![](../images/2dTinkerCAD/arduinoHole2.png)
Copy paste the 3.2 mm hole and relocate them.
![](../images/2dTinkerCAD/arduinoHole3.png)
![](../images/2dTinkerCAD/arduinoHole4.png)
Copy paste the 3.2 mm again to fit them with an Arduino Uno board.

## Filet
![](../images/2dTinkerCAD/3mmBox.png)
Create a 3mm box
![](../images/2dTinkerCAD/6mmCylinder.png)
Create a 6mm hole cylinder
![](../images/2dTinkerCAD/align1.png)
![](../images/2dTinkerCAD/align.png)
Align the 6 mm cylinder to the corner of the 3mm box.
![](../images/2dTinkerCAD/filet.png)
Combine the cylinder with the box and change it into the hole mode.
![](../images/2dTinkerCAD/filetResult.png)
Duplicate them and put the filet hole to the four corner of the working piece using the aligment tool.
![](../images/2dTinkerCAD/result.png)
This is the final result

## Downloads

<a href="../images/tinkerCAD/Pinguin.stl" download></a>

## Useful Links
-   [TinkerCAD](https://www.tinkercad.com/)


</div>
