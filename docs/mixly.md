# Mixly
## Introduction
Arduino is a very popular platform since its being Open Source in terms of both software and hardware. More and more people want to learn Arduino programming. But once they open the software Arduino IDE to check the samples, the complicated code frustrates them. For those without programming basics, Arduino programming is definitely a big obstacle. Fortunately, you have Mixly – a free, open source, graphical visual programming language software based on Arduino, which will make programming as easy as building blocks.


![](../images/mixly/mixly.png)

## Objectives

- Definitions of embedded systems
- Sensors and Actuators
- Digital and Analog
- Embedeed programming


## Embedded Systems
Embedded system is a very simple computer, it can process only electric signals (a bit like the electric pulses that are sent between neurons in our brains). 

## Output Devices (Actuators)

An output device is an hardware device able to produce a physical output given a specific electrical signal. The output changes accordingly to the electrical signal in input, that can be generated from a computer. Any kind of electrical actuators can be considered as output devices but also a printer, a speaker and so on. A microcontroller is able to generate the proper electrical signals to control a wide range of output devices.

### LED

  Turns an LED on for one second, then off for one second, repeatedly.



![](../images/mixly/blinkingLED.png)



```

/*
  Blink


  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}

```


![](../images/mixly/blinkingLED.gif)

### Servo Motor
#### Block Icon
![](../images/mixly/servo.png)
#### Definition
- Sets the servo pin.
- Moves between 0-180 degree.
- Delay time for servo to rotate.

#### Example
Connect the signal end of servo to Digital 3 of Uno, then upload the code below, servo will rotate from 0 degrees to 190 degrees and repeat.
Note: Delay 1500 ms is the time required for servo to move.
![](../images/mixly/servo0_180.png)

```

#include <Servo.h>

Servo servo_3;

void setup(){
  servo_3.attach(3);
}

void loop(){
  servo_3.write(0);
  delay(1500);
  servo_3.write(180);
  delay(1500);
}

```

![](../images/mixly/servo_motor.gif)

### Buzzer Block
Connection of a buzzer block /Piezo speaker to pin 3

concept.jpeg


<video width="600" controls>
  <source src="../images/mixly/buzzer.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

### LCD1602

Separately connect the SDA (A4) and SCL (A5) of Arduino Uno to SDA and SCL pins of IIC LCD1602, then set the address of your LCD1602 screen, the LCD address we used here is 0x27. Then upload the code, LCD screen has two lines, you should see the line 1 print "HELLO", and line 2 print "Fab Lab O Shanghai".

If you have not installaled the library import this following file.
To use the library in your own sketch, select it from Sketch > Import Library in Arduino IDE.
https://www.arduinolibraries.info/libraries/liquid-crystal-i2-c

![](../images/mixly/lcd_mixly.png)

```
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C mylcd(0x27,16,2);

void setup(){
  mylcd.init();
  mylcd.backlight();
}

void loop(){
  mylcd.setCursor(0,0);
  mylcd.print("Fab Lab O");
  mylcd.setCursor(0, 1);
  mylcd.print("");
}

```

How to install a librarie: 
https://www.arduino.cc/en/guide/libraries


![](../images/mixly/lcd.png)

### DC Motor

![](../images/mixly/motor.png)

```

void front(){
  digitalWrite(4,HIGH);
  analogWrite(5,200);
}

void back(){
  digitalWrite(4,LOW);
  analogWrite(5,200);
}

void stop(){
  digitalWrite(4,LOW);
  analogWrite(5,0);
}

void setup(){
  pinMode(4, OUTPUT);
}

void loop(){
  front();
  delay(1000);
  back();
  delay(1000);
  stop();
  delay(1000);
}

```

![](../images/mixly/dc_motor.gif)

Note: Digital pin 4 and analog pin 5 are the pair for one motor, and digital pin 7 and analog pin 6 are for the other motor.

## Input Devices (Sensors)
An input device is an hardware device able to produce an electrical signal depending on a physical input. The electrical signal changes accordingly to the changes of the input and can be read from a computer. All the sensors can be considered input devices, but also a camera, a keyboard, a mouse and so on. Due to the presence of an analog-digital converter and dedicated functions a microcontroller is well suited to directly read data from sensors.

## Digital Input

### Button

Turn on LED when the button is pressed


![](../images/mixly/button1.png)

```

/*
  Turn on LED when the button is pressed
*/

int value;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, INPUT);
  pinMode(12, OUTPUT)
}

// the loop function runs over and over again forever
void loop() {

  value = digitalRead(12);

  // check if the button is pressed (input is LOW)

  if(value == HIGH){
    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)

  }else{
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW

  }
  
}

```

![](../images/mixly/button1.gif)


### Obstacle Sensor

![](../images/mixly/obstacle_sensor.png)


```

int value;

// the setup function runs once when you press reset or power the board
void setup() {

  Serial.begin(9600);
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, INPUT);
  pinMode(8, OUTPUT)

  value = 0;
}

// the loop function runs over and over again forever
void loop() {

  value = digitalRead(8);

  // check if the button is pressed (input is LOW)

  if(value == 0){
    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)

  }else{
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW

  }
  
}
```

![](../images/mixly/obstacle_sensor.gif)


## Ultrasonic
Connect the Trig pin of ultrasonic sensor to Digital 8 of Uno, Echo pin to D9, then upload the code and open the monitor, you should see the distance value, updating once per 100ms.


![](../images/mixly/ultrasonic.png)


```
int distance= 0; //variable to store the value coming from the sensor

float checkdistance_8_9(){
  digitalWrite(8, LOW);
  delayMicroseconds(2);
  digitalWrite(8, HIGH);
  delayMicroseconds(10);
  digitalWrite(8, LOW);
  float distance = pulseIn(9, HIGH) / 58.00;
  delay(10);
  return distance


}

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600); // open serial port
  distance = 0;
  pinMode(8 , OUTPUT);
  pinMode(9, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  distance = checkdistance_8_9();
  Serial.print("distance: ")
  Serial.println(distance); //  print the value to the serial port
  Serial.print("cm")
  delay(100); // wait 100 ms between each send
}

```

![](../images/mixly/ultrasonic.gif)


## Analog Input

### Potentiometer

Read the value from a potentiometer and print to the serial monitor

![](../images/mixly/potentiometer.png)

```
int val= 0; //variable to store the value coming from the sensor

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600); // open serial port

}

void loop() {
  // put your main code here, to run repeatedly:
  val = analogRead(A0);// read the value from the sensor
  Serial.println(val); //  print the value to the serial port
  delay(100); // wait 100 ms between each send
}
```


![](../images/mixly/potentiometer.gif)


### Temperature

![](../images/mixly/temp.png)

```
int val= 0; //variable to store the value coming from the sensor

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600); // open serial port

}

void loop() {
  // put your main code here, to run repeatedly:
  val = analogRead(A0);// read the value from the sensor
  Serial.println(val); //  print the value to the serial port
  delay(100); // wait 100 ms between each send
}
```
![](../images/mixly/temp.gif)


#### Import Library

1. Download keystudio.zip : https://drive.google.com/drive/folders/1oXmG0ywhsFf9zRqCB_0kuTaQg_xcXRG2

2.  Extract the keystudio.zip
3.  Open Mixly
4.  Click Import
  ![](../images/mixly/import.png)
5.  Select keystudio.xml and click open
![](../images/mixly/keystudio.png)


Block code:

![](../images/mixly/temperature.png)


```
int val;

void setp(){
  Serial.begin(9600);
  val = 0;
  pinMode(A0, INPUT);
  
}

void loop(){
  val = analogRead(A0)*0.488;
  Serial.print("temp : ");
  Serial.print(val);
  Serial.println("°C")
  delay(100);

}

```

![](../images/mixly/temperature.gif)

# Communication

## IR Remote Control

```
#include <IRremote.h>

long ir_item;

IRrecv irrecv(12);
decode_results results;

void setup()
{
  Serial.begin(9600);
  // In case the interrupt driver crashes on setup, give a clue
  // to the user what's going on.
  Serial.println("Enabling IRin");
  irrecv.enableIRIn(); // Start the receiver
  Serial.println("Enabled IRin");
}

void loop() {
  if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);
    irrecv.resume(); // Receive the next value
  }
  delay(100);
}
```

[Arduino IRremote library](https://github.com/z3t0/Arduino-IRremote)



## Useful Links
-   [Getting started with Mixly (Wiki)](https://wiki.keyestudio.com/Getting_Started_with_Mixly#Actuator_Block/)

=======
# Mixly
## Introduction|简介
Arduino is a very popular platform since its being Open Source in terms of both software and hardware. More and more people want to learn Arduino programming. But once they open the software Arduino IDE to check the samples, the complicated code frustrates them. For those without programming basics, Arduino programming is definitely a big obstacle. Fortunately, you have Mixly – a free, open source, graphical visual programming language software based on Arduino, which will make programming as easy as building blocks.


![](../images/mixly/mixly.png)

## Objectives|内容

- Definitions of embedded systems|什么是嵌入式系统
- Sensors and Actuators|传感器、执行器
- Digital and Analog|数字信号和模拟信号
- Embedeed programming|嵌入式系统编程


## Embedded Systems|嵌入式系统
Embedded system is a very simple computer, it can process only electric signals (a bit like the electric pulses that are sent between neurons in our brains). 

## Output Devices (Actuators)|输出设备（执行器）

An output device is an hardware device able to produce a physical output given a specific electrical signal. The output changes accordingly to the electrical signal in input, that can be generated from a computer. Any kind of electrical actuators can be considered as output devices but also a printer, a speaker and so on. A microcontroller is able to generate the proper electrical signals to control a wide range of output devices.

### LED

  Turns an LED on for one second, then off for one second, repeatedly.



![](../images/mixly/blinkingLED.png)



```

/*
  Blink


  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}

```


![](../images/mixly/blinkingLED.gif)

### Servo Motor|舵机
#### Block Icon|模块图标
![](../images/mixly/servo.png)
#### Definition|定义
- Sets the servo pin.
- Moves between 0-180 degree.
- Delay time for servo to rotate.

#### Example|示例
Connect the signal end of servo to Digital 3 of Uno, then upload the code below, servo will rotate from 0 degrees to 190 degrees and repeat.
Note: Delay 1500 ms is the time required for servo to move.
![](../images/mixly/servo0_180.png)

```

#include <Servo.h>

Servo servo_3;

void setup(){
  servo_3.attach(3);
}

void loop(){
  servo_3.write(0);
  delay(1500);
  servo_3.write(180);
  delay(1500);
}

```

![](../images/mixly/servo_motor.gif)

### Buzzer Block|蜂鸣器模块
Connection of a buzzer block /Piezo speaker to pin 3

![](../images/mixly/buzzer.png)


<video width="600" controls>
  <source src="../images/mixly/buzzer.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

### LCD1602|液晶显示器

Separately connect the SDA (A4) and SCL (A5) of Arduino Uno to SDA and SCL pins of IIC LCD1602, then set the address of your LCD1602 screen, the LCD address we used here is 0x27. Then upload the code, LCD screen has two lines, you should see the line 1 print "HELLO", and line 2 print "Fab Lab O Shanghai".

If you have not installaled the library import this following file.
To use the library in your own sketch, select it from Sketch > Import Library in Arduino IDE.
https://www.arduinolibraries.info/libraries/liquid-crystal-i2-c

![](../images/mixly/lcd_mixly.png)

```
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C mylcd(0x27,16,2);

void setup(){
  mylcd.init();
  mylcd.backlight();
}

void loop(){
  mylcd.setCursor(0,0);
  mylcd.print("Fab Lab O");
  mylcd.setCursor(0, 1);
  mylcd.print()
}

```

How to install a librarie: 
https://www.arduino.cc/en/guide/libraries


![](../images/mixly/lcd.png)

### DC Motor|直流电机

![](../images/mixly/motor.png)

```

void front(){
  digitalWrite(4,HIGH);
  analogWrite(5,200);
}

void back(){
  digitalWrite(4,LOW);
  analogWrite(5,200);
}

void stop(){
  digitalWrite(4,LOW);
  analogWrite(5,0);
}

void setup(){
  pinMode(4, OUTPUT);
}

void loop(){
  front();
  delay(1000);
  back();
  delay(1000);
  stop();
  delay(1000);
}

```

![](../images/mixly/dc_motor.gif)

Note: Digital pin 4 and analog pin 5 are the pair for one motor, and digital pin 7 and analog pin 6 are for the other motor.

## Input Devices (Sensors)|输入设备（传感器）
An input device is an hardware device able to produce an electrical signal depending on a physical input. The electrical signal changes accordingly to the changes of the input and can be read from a computer. All the sensors can be considered input devices, but also a camera, a keyboard, a mouse and so on. Due to the presence of an analog-digital converter and dedicated functions a microcontroller is well suited to directly read data from sensors.

## Digital Input|数字信号输入

### Button|按钮

Turn on LED when the button is pressed


![](../images/mixly/button1.png)

```

/*
  Turn on LED when the button is pressed
*/

int value;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, INPUT);
  pinMode(12, OUTPUT)
}

// the loop function runs over and over again forever
void loop() {

  value = digitalRead(12);

  // check if the button is pressed (input is LOW)

  if(value == HIGH){
    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)

  }else{
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW

  }
  
}

```

![](../images/mixly/button1.gif)


### Obstacle Sensor|红外避障传感器

![](../images/mixly/obstacle_sensor.png)


```

int value;

// the setup function runs once when you press reset or power the board
void setup() {

  Serial.begin(9600);
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, INPUT);
  pinMode(8, OUTPUT)

  value = 0;
}

// the loop function runs over and over again forever
void loop() {

  value = digitalRead(8);

  // check if the button is pressed (input is LOW)

  if(value == 0){
    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)

  }else{
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW

  }
  
}
```

![](../images/mixly/obstacle_sensor.gif)


## Ultrasonic|超声波传感器
Connect the Trig pin of ultrasonic sensor to Digital 8 of Uno, Echo pin to D9, then upload the code and open the monitor, you should see the distance value, updating once per 100ms.


![](../images/mixly/ultrasonic.png)


```
int distance= 0; //variable to store the value coming from the sensor

float checkdistance_8_9(){
  digitalWrite(8, LOW);
  delayMicroseconds(2);
  digitalWrite(8, HIGH);
  delayMicroseconds(10);
  digitalWrite(8, LOW);
  float distance = pulseIn(9, HIGH) / 58.00;
  delay(10);
  return distance


}

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600); // open serial port
  distance = 0;
  pinMode(8 , OUTPUT);
  pinMode(9, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  distance = checkdistance_8_9();
  Serial.print("distance: ")
  Serial.println(distance); //  print the value to the serial port
  Serial.print("cm")
  delay(100); // wait 100 ms between each send
}

```

![](../images/mixly/ultrasonic.gif)


## Analog Input|模拟输入

### Potentiometer|电位器/滑动变阻器

Read the value from a potentiometer and print to the serial monitor

![](../images/mixly/potentiometer.png)

```
int val= 0; //variable to store the value coming from the sensor

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600); // open serial port

}

void loop() {
  // put your main code here, to run repeatedly:
  val = analogRead(A0);// read the value from the sensor
  Serial.println(val); //  print the value to the serial port
  delay(100); // wait 100 ms between each send
}
```


![](../images/mixly/potentiometer.gif)


### Temperature|温度传感器

![](../images/mixly/temp.png)

```
int val= 0; //variable to store the value coming from the sensor

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600); // open serial port

}

void loop() {
  // put your main code here, to run repeatedly:
  val = analogRead(A0);// read the value from the sensor
  Serial.println(val); //  print the value to the serial port
  delay(100); // wait 100 ms between each send
}
```
![](../images/mixly/temp.gif)


#### Import Library|导入库文件

1. Download keystudio.zip : https://drive.google.com/drive/folders/1oXmG0ywhsFf9zRqCB_0kuTaQg_xcXRG2

2.  Extract the keystudio.zip
3.  Open Mixly
4.  Click Import
  ![](../images/mixly/import.png)
5.  Select keystudio.xml and click open
![](../images/mixly/keystudio.png)


Block code:

![](../images/mixly/temperature.png)


```
int val;

void setp(){
  Serial.begin(9600);
  val = 0;
  pinMode(A0, INPUT);
  
}

void loop(){
  val = analogRead(A0)*0.488;
  Serial.print("temp : ");
  Serial.print(val);
  Serial.println("°C")
  delay(100);

}

```

![](../images/mixly/temperature.gif)

# Communication|通信

## IR Remote Control|红外遥控

```
#include <IRremote.h>

long ir_item;

IRrecv irrecv(12);
decode_results results;

void setup()
{
  Serial.begin(9600);
  // In case the interrupt driver crashes on setup, give a clue
  // to the user what's going on.
  Serial.println("Enabling IRin");
  irrecv.enableIRIn(); // Start the receiver
  Serial.println("Enabled IRin");
}

void loop() {
  if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);
    irrecv.resume(); // Receive the next value
  }
  delay(100);
}
```

[Arduino IRremote library](https://github.com/z3t0/Arduino-IRremote)



## Useful Links|参考链接
-   [Getting started with Mixly (Wiki)](https://wiki.keyestudio.com/Getting_Started_with_Mixly#Actuator_Block/)

