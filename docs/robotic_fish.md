# Robotic Fish
![](../images/bio_fish/sketch.png)
## Part List
- 1 x Mixly board
- 1 x IR remote control
- 1 x 5AA Battery pack
- 2 x Servo motors
- 1 x Styrofoam 2 cm
- 1 x Choopstick
- 1 x fish fin tail (could be made by acrylic)

## Machine
- Glue gun
- Laser cutter
- Styrofoam cutter

## Model
![](../images/bio_fish/model1.JPG)
![](../images/bio_fish/model2.JPG)

## Programming

The robotic fish with IR remote control.
```
/*
 * Robotic Fish with IR remote control
 * Marcello Tania
 * 06/06/2020
 */

#include <IRremote.h>
#include <Servo.h>


long ir_item;

IRrecv irrecv(12);
decode_results results;

Servo servo_3;
Servo servo_9;

void left();
void right();
void middle();
void forward();



void setup()
{
  Serial.begin(9600);
  // In case the interrupt driver crashes on setup, give a clue
  // to the user what's going on.
  Serial.println("Enabling IRin");
  irrecv.enableIRIn(); // Start the receiver
  Serial.println("Enabled IRin");

  servo_3.attach(3);
  servo_9.attach(9);
}

void loop() {
  if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);
    ir_item = results.value;
    
    switch (ir_item) {
      case 0xFFE01F:
        left();
        break;
      case 0xFF906F:
        right();
        break;
      case 0xFFA857:
        middle();
        break;
      case 0xFF02FD:
        forward();
        break;
    }
    
    irrecv.resume(); // Receive the next value
  }
  delay(100);
}

void left(){
  Serial.println("Left");
  servo_3.write(0);
  delay(500);
}

void right(){
  Serial.println("Right");
  servo_3.write(130);
  delay(500);
}

void middle(){
  Serial.println("Middle");
  servo_3.write(90);
  delay(500);
}

void forward(){
  Serial.println("Forward");
  for(int i = 0;i <= 2;i++){
    servo_9.write(100);
    delay(500);
    servo_9.write(150);
    delay(500);
  }
}
```

# Compas Sensor

https://github.com/sleemanj/HMC5883L_Simple

# Media

![](../images/bio_fish/eva.png)
![](../images/bio_fish/inkscape.jpeg)
![](../images/bio_fish/eva2.JPG)
![](../images/bio_fish/testing.jpeg)
![](../images/bio_fish/demo.JPG)
![](../images/bio_fish/graduation.jpeg)
## Useful links

- [GitLab docs](https://docs.gitlab.com/ee/ssh/#adding-an-ssh-key-to-your-gitlab-account)


