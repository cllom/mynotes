# Hand Washing Timer
![](../images/washHand/demo1.gif)
## Introduction
New life after the lockdown during the pandemic needs something innovative to support our society to save someone life.

While we’re still waiting for a vaccine against the coronavirus, one of the best ways we can protect ourselves is by consent hand-washing.Well, we should always be washing our hands with or without a pandemic, but some people either forget or consider it a bother. But now that it can actually mean life or death, people are paying close attention to this hygienic habit.

The recommended time for washing your hands with soap is 20 seconds.

## Concept

A reminder for the public to wash our hand for at least 20 seconds.

![](../images/washHand/concept.jpeg)
Concept design by Valerie Katia

## Proof of Concent
### Video

![](../images/washHand/proofOfConcept.gif)

![](../images/washHand/concept.gif)

![](../images/washHand/prototype_v1.gif)

![](../images/washHand/folcano1.JPG)

<iframe width="560" height="315" src="https://www.youtube.com/embed/hOeEMogRXkE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Programming Code

```
/*

  Marcello Tania 19/06/2020

  This work may be reproduced, modified, distributed,
  performed, and displayed for any purpose. Copyright is
  retained and must be preserved. The work is provided
  as is; no warranty is provided, and users accept all 
  liability.
*/

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define pin A0

LiquidCrystal_I2C mylcd(0x27,16,2);
void idle();
uint16_t get_gp2d12 (uint16_t value);
void timer(int number);


void setup(){
  mylcd.init();
  mylcd.backlight();
  idle();

  Serial.begin (9600);
  pinMode (pin, INPUT);
}

void loop(){
  uint16_t value = analogRead (pin);
  uint16_t range = get_gp2d12 (value);

  if(range<150){

      int num=20;  //initializing the variable
      do  //do-while loop 
      {
        num--;    //decremanting operation
        Serial.println(num); 
        timer(num);
        delay(1000);
      }while(num>0);
  }

  idle();

}

void idle(){
  mylcd.setCursor(0,0);
  //mylcd.print("Lambaikan tangan");
  mylcd.print(" Wave your hand");
  mylcd.setCursor(0, 1);
  //mylcd.print("   untuk mulai");
  mylcd.print("    to start");
}

void timer(int number){
   mylcd.setCursor(0,0);
   mylcd.print("       ");
   mylcd.print(number);
   mylcd.print("       ");
   mylcd.setCursor(0, 1);
   //mylcd.print("   detik lagi   ");
   mylcd.print("     seconds   ");
}

uint16_t get_gp2d12 (uint16_t value) {
    if (value < 10) value = 10;
    return ((67870.0 / (value - 3.0)) - 40.0);
}
```

# PCB

![](../images/washHand/pcb.jpeg)
![](../images/washHand/pcbDemo.gif)

# Casing

## 3D Printed Casing
![](../images/washHand/casing.gif)

## After polished
![](../images/washHand/polished.gif)


# Future Works

## Applications
### Security
![](../images/washHand/useCase1.JPG)

### Payment menthod
![](../images/washHand/payment.JPG)

### HD Display
![](../images/washHand/HDDisplay_Folcano.gif)

![](../images/washHand/fablab.gif)









