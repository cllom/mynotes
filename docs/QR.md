# QRcode

## Test Your Imports 测试引用包

```
import cv2
import numpy as np
```

## Scan a QR Code


```
import cv2
import numpy as np

cap = cv2.VideoCapture(0)
qrDecoder = cv2.QRCodeDetector()

while True:
    _, frame = cap.read()
    # Detect and decode the qrcode
    data,bbox,rectifiedImage = qrDecoder.detectAndDecode(frame)
    if len(data)>0:
        print("Decoded Data : {}".format(data))
        
        cv2.putText(frame, format(data), tuple(bbox[0][0]), cv2.FONT_HERSHEY_COMPLEX, 2, (100,170,0), 3)

        # Display barcode and QR code location
        print(bbox)
        cv2.line(frame, tuple(bbox[0][0]), tuple(bbox[0][1]), (255,0,0), 3)
        cv2.line(frame, tuple(bbox[0][1]), tuple(bbox[0][2]), (255,0,0), 3)
        cv2.line(frame, tuple(bbox[0][2]), tuple(bbox[0][3]), (255,0,0), 3)
        cv2.line(frame, tuple(bbox[0][0]), tuple(bbox[0][3]), (255,0,0), 3)

    else:
        print("QR Code not detected")

    cv2.imshow("Frame", frame)

    key = cv2.waitKey(1)
    if key == 27:
        break

# When everything is done, release the capture 完成所有工作后，释放摄像头
cap.release()
cv2.destroyAllWindows()
```

## Open a web browser
```
import webbrowser

url = 'http://marcellotania.com/'

# MacOS
chrome_path = 'open -a /Applications/Google\ Chrome.app %s'

# Windows
# chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'

# Linux
# chrome_path = '/usr/bin/google-chrome %s'

webbrowser.get(chrome_path).open(url)
```
## Scan a QR code and open a web browser
```
import cv2
import numpy as np
import webbrowser

cap = cv2.VideoCapture(1)
qrDecoder = cv2.QRCodeDetector()

while True:
    _, frame = cap.read()
    # Detect and decode the qrcode
    data,bbox,rectifiedImage = qrDecoder.detectAndDecode(frame)
    if len(data)>0:
        print("Decoded Data : {}".format(data))
        
        # Display barcode and QR code location
        print(bbox)
        cv2.line(frame, tuple(bbox[0][0]), tuple(bbox[0][1]), (255,0,0), 3)
        cv2.line(frame, tuple(bbox[0][1]), tuple(bbox[0][2]), (255,0,0), 3)
        cv2.line(frame, tuple(bbox[0][2]), tuple(bbox[0][3]), (255,0,0), 3)
        cv2.line(frame, tuple(bbox[0][0]), tuple(bbox[0][3]), (255,0,0), 3)
        
        url = format(data)

        # MacOS
        chrome_path = 'open -a /Applications/Google\ Chrome.app %s'

        # Windows
        # chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'

        # Linux
        # chrome_path = '/usr/bin/google-chrome %s'

        webbrowser.get(chrome_path).open(url)
        break

    else:
        print("QR Code not detected")

    cv2.imshow("Frame", frame)

    key = cv2.waitKey(1)
    if key == 27:
        break
        
# When everything is done, release the capture 完成所有工作后，释放摄像头
cap.release()
cv2.destroyAllWindows()
```

## Useful Links
- [OpenCV Documentation](https://docs.opencv.org/master/)

