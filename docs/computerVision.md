# Computer vision 计算机视觉

## Test Your Imports 测试引用包

```
import cv2
import numpy as np
import matplotlib
```

## Drawing images and shapes using OpenCV 使用OpenCV绘制图像和图形

Let's start off my making a black square
让我们首先画一个黑色方块

```
import cv2
import numpy as np

# Create a black image 创建一个黑色图像
image = np.zeros((512,512,3), np.uint8)

# Can we make this in black and white? 
image_bw = np.zeros((512,512), np.uint8)

cv2.imshow("Black Rectangle (Color)", image)
cv2.imshow("Black Rectangle (B&W)", image_bw)

cv2.waitKey(0)
cv2.destroyAllWindows()
```

![](./assets/images/computerVision/blackSquare.png)

## Let's draw a line over our black square 在黑色方块的基础上画一条线
```
cv2.line(image, starting cordinates, ending cordinates, color, thickness)
```

# Draw a diagonal blue line of thickness of 5 pixels 画一条宽度为5个像素的蓝色斜线
```
image = np.zeros((512,512,3), np.uint8)
cv2.line(image, (0,0), (511,511), (255,127,0), 5)
cv2.imshow("Blue Line", image)

cv2.waitKey(0)
cv2.destroyAllWindows()
```
![](./assets/images/computerVision/line.png)

## Let's now draw a rectangle 画一个矩形
cv2.rectangle(image, starting vertex, opposite vertex, color, thickness)
```
# Draw a Rectangle in
image = np.zeros((512,512,3), np.uint8)

cv2.rectangle(image, (100,100), (300,250), (127,50,127), -1)
cv2.imshow("Rectangle", image)
cv2.waitKey(0)
cv2.destroyAllWindows()
```

![](./assets/images/computerVision/rectangle.png)


### How about cirlcle? 画个圆？
cv2.cirlce(image, center, radius, color, fill)
```
image = np.zeros((512,512,3), np.uint8)

cv2.circle(image, (350, 350), 100, (15,75,50), -1) 
cv2.imshow("Circle", image)
cv2.waitKey(0)
cv2.destroyAllWindows()
```

![](./assets/images/computerVision/circle.png)

### Let's even add text with cv2.putText 使用cv2.putText来添加文字

cv2.putText(image, 'Text to Display', bottom left starting point, Font, Font Size, Color, Thickness)

- FONT_HERSHEY_SIMPLEX, FONT_HERSHEY_PLAIN
- FONT_HERSHEY_DUPLEX,FONT_HERSHEY_COMPLEX 
- FONT_HERSHEY_TRIPLEX, FONT_HERSHEY_COMPLEX_SMALL
- FONT_HERSHEY_SCRIPT_SIMPLEX
- FONT_HERSHEY_SCRIPT_COMPLEX
  
```
image = np.zeros((512,512,3), np.uint8)

cv2.putText(image, 'Hello Cello!', (75,290), cv2.FONT_HERSHEY_COMPLEX, 2, (100,170,0), 3)
cv2.imshow("Hello World!", image)
cv2.waitKey(0)
cv2.destroyAllWindows()
```
![](./assets/images/computerVision/helloWorld.png)

## Capture Video from Camera 使用摄像头来捕捉视屏

```
import cv2
import numpy as np

# Open Webcam 打开摄像头
# 0 is camera device number, 0 is for internal webcam and 1 will access the first connected usb webcam
# 参数0是摄像设备号码，0表示内置摄像头，1表示第一个外接USB摄像头
cap = cv2.VideoCapture(0) 

while True:
    
    # Capture frame-by-frame 逐帧获取
    ret, frame = cap.read()
    
    # mirror the frame 将每一帧转换镜像
    frame = cv2.flip(frame, 1)
    
    # Display the resulting frame 展示结果帧
    cv2.imshow('Video', frame)
    
    # Press Q on keyboard to  exit 按键‘Q’退出程序
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
        

# When everything is done, release the capture 完成所有工作后，释放摄像头
cap.release()
cv2.destroyAllWindows()

```


## Face and Eye Detection 人脸与眼部检测

```
import cv2
import numpy as np


face_classifier = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
eye_classifier = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_eye.xml')


# Open Webcam 打开摄像头
# 0 is camera device number, 0 is for internal webcam and 1 will access the first connected usb webcam
cap = cv2.VideoCapture(0) 


def face_detector(img):
    
    
    
    # Convert image to grayscale 将图像转化为灰度
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    
    # Clasify face from the gray image 使用灰度图片将人脸进行分类
    faces = face_classifier.detectMultiScale(gray, 1.3, 5)
    if faces is ():
        return img, []
    
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,255),2)
        xCentre= int(x+(w/2))
        yCentre= int(y+(w/2))
        cv2.circle(img, (xCentre, yCentre), 5, (0,255,255), -1 )
        
        localization=[xCentre,yCentre]
        
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_classifier.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(255,255,0),2)
        
        #print(localization)
    img = cv2.flip(roi_color,1)
    return img, localization

while True:
    
    # Capture frame-by-frame 逐帧处理
    ret, frame = cap.read()
    
    
    # Set camera resolution. The max resolution is webcam dependent 设置分辨率，最大分辨率取决于摄像头
    # so change it to a resolution that is both supported by your camera 
    # and compatible with your monitor 将分辨率设置为摄像头和屏幕都适合的值
    #cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    #cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 800)

    # mirror the frame 将每一帧转换镜像
    frame = cv2.flip(frame, 1)
    
    image, face = face_detector(frame)
    #print(face)
    
    
    # Display the resulting frame 展示结果帧
    cv2.imshow('Video', frame)
    

    # Press Q on keyboard to  exit 按键‘Q’退出程序
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
        

# When everything is done, release the capture 完成所有工作后，释放摄像头资源
cap.release()
cv2.destroyAllWindows()
```

![](./assets/images/computerVision/faceDetection.gif)


# Recognizing faces 人脸识别

```
import face_recognition
import cv2
import numpy as np

# This is a demo of running face recognition on live video from your webcam. It's a little more complicated than the
# other example, but it includes some basic performance tweaks to make things run a lot faster:
#   1. Process each video frame at 1/4 resolution (though still display it at full resolution)
#   2. Only detect faces in every other frame of video.

# 这是一个使用自己摄像头拍摄的实时视屏进行人脸识别的演示程序。会比之前的例子更复杂一些，其中包含了一些可以提高性能和运行速度的技巧：
# 1. 使用视屏的1/4分辨率进行数据处理（但在展示时仍然使用最大分辨率）；
# 2. 每隔一帧才检测人脸

# PLEASE NOTE: This example requires OpenCV (the `cv2` library) to be installed only to read from your webcam.
# OpenCV is *not* required to use the face_recognition library. It's only required if you want to run this
# specific demo. If you have trouble installing it, try any of the other demos that don't require it instead.

# Get a reference to webcam #0 (the default one)
video_capture = cv2.VideoCapture(0)

# Load a sample picture and learn how to recognize it. 加载一张样例图片然后学习如何识别它
obama_image = face_recognition.load_image_file("obama.jpg")
obama_face_encoding = face_recognition.face_encodings(obama_image)[0]

# Load a second sample picture and learn how to recognize it. 再加载一张样例图片
biden_image = face_recognition.load_image_file("cello.jpg")
biden_face_encoding = face_recognition.face_encodings(biden_image)[0]

# Create arrays of known face encodings and their names 创建已知人脸数据数组和名称数组
known_face_encodings = [
    obama_face_encoding,
    biden_face_encoding
]
known_face_names = [
    "Barack Obama",
    "Cello"
]

# Initialize some variables 初始化变量
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

while True:
    # Grab a single frame of video 从摄像头获取帧数据
    ret, frame = video_capture.read()

    # Resize frame of video to 1/4 size for faster face recognition processing 调整获取帧的大小到1/4
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses) 调整颜色模式，从BGR转换到RGB（face_recognition程序使用）
    rgb_small_frame = small_frame[:, :, ::-1]

    # Only process every other frame of video to save time 每隔一帧处理数据以节省事件
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video 解析获取出当前帧的脸部数据
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s) 看是否为已知的脸部数据
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
            name = "Unknown"

            # # If a match was found in known_face_encodings, just use the first one.
            # # 如果再已知脸部数据known_face_encodings中找到匹配，则直接使用
            # if True in matches:
            #     first_match_index = matches.index(True)
            #     name = known_face_names[first_match_index]

            # Or instead, use the known face with the smallest distance to the new face 
            # 否则使用与新数据距离最近的已知脸部数据
            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]

            face_names.append(name)

    process_this_frame = not process_this_frame


    # Display the results 结果展示
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # Draw a box around the face 在脸部周围画一个框
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # Draw a label with a name below the face 在脸部下发画一个名字
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    # Display the resulting image 展示结果图像
    cv2.imshow('Video', frame)

    # Hit 'q' on the keyboard to quit! 按键‘Q’退出程序
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release handle to the webcam 释放摄像头资源
video_capture.release()
cv2.destroyAllWindows()
```

![](./assets/images/computerVision/faceRecognition.gif)


# Face Mask 脸部遮罩

```
import cv2
import numpy as np

face_classifier = cv2.CascadeClassifier('Haarcascades/haarcascade_frontalface_default.xml')

#Open Webcam
# 0 is camera device number, 0 is for internal webcam and 1 will access the first connected usb webcam
cap = cv2.VideoCapture(0)

def face_detector(img):
    global face_mask
    
    # Convert image to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    
    # Clasify face from the gray image
    faces = face_classifier.detectMultiScale(gray, 1.3, 5)
    if faces is ():
        return img, []
    
    for (x,y,w,h) in faces:
        #cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,255),2)
        xCentre= int(x+(w/2))
        yCentre= int(y+(w/2))
        #cv2.circle(img, (xCentre, yCentre), 5, (0,255,255), -1 )

        localization=[xCentre,yCentre]
        # Position
        top_left = (x,y)
        bottom_right = (x+w,y+h)
        
        # Adding the new face
        face_mask = cv2.resize(face_mask, (w, h))
        face_mask_gray = cv2.cvtColor(face_mask, cv2.COLOR_BGR2GRAY)
        _, face_mask_thres = cv2.threshold(face_mask_gray, 25, 255, cv2.THRESH_BINARY_INV)

        face_area = frame[top_left[1]: top_left[1] + h, top_left[0]: top_left[0] + w]
        face_area_no_face = cv2.bitwise_and(face_area, face_area, mask= face_mask_thres)
        
        final_face = cv2.addWeighted(face_area_no_face,1, face_mask,0.7,0)
        frame[top_left[1]: top_left[1] + h, top_left[0]: top_left[0] + w] = final_face

        #cv2.imshow("face_mask", face_mask)
        #cv2.imshow("face_area", face_area)
        #cv2.imshow("face area no face", face_area_no_face)
        #cv2.imshow("final face", final_face)

        #print(localization)
    return img, localization

while True:

    # Loading the image
    face_mask = cv2.imread("iron_man.png")
    
    # Capture frame by frame
    
    ret,frame = cap.read()
    
    # mirror the frame
    frame = cv2.flip(frame, 1)
    
    image, face = face_detector(frame)
    
    # Display the resulting frame
    cv2.imshow('Video', frame)
    
    # Pres Q on keyboard to exit
    if cv2.waitKey(25) & 0xFF ==ord('q'):
        cv2.destroyAllWindows()
        break
        
# When everything is done, release the capture
cap.release()
cv2.destroyAllWindows()
```

![](./assets/images/computerVision/face_mask.gif)

## Downloads
- [Haarscade](https://fabocloud.cn/fab/2020/summerbootcamp/iron-man/bootcamp01/materials)



## Useful Links
- [OpenCV Documentation](https://docs.opencv.org/master/)
- [Face recognition with OpenCV, Python, and deep learning](https://www.pyimagesearch.com/2018/06/18/face-recognition-with-opencv-python-and-deep-learning/)
