---
layout: page
title: PID
permalink: /PID
---

# PID Contol

## What is PID Control?

Generally, I won't start dealing with a lot of math to learn something new in control theory. Instead, first, we are focusing on building a good foundation! 

![](./assets/images/invertedPendulum/pidEquation.jpg)

Let us begin! We would start what I call a 'plant'. This is a system we want to control. The input of the plant is the 'acuating signal' and the output is the 'controlled variable'. Different textbooks or people might give you different names. For instance, someone says 'plant input' and 'plant output'. Regardless, of what the names are. The idea is to generate the actuating signal so that we get is what want for the controlled variable. 

![](./assets/images/invertedPendulum/plant.jpg)


The job of the control engineer is to get the input to the plant and get the output of what you want. To do that, we would have a 'command variable' which is an input signal to the system, to tell what we want the system to do. Next is getting a feedback signal from the system. Comparing those two variables will give us the 'error signal'. It is how far off the system is. The goal is to have a zero error meaning that the feedback signal we get from a sensor is how want it.

![](./assets/images/invertedPendulum/error.jpg)


# Controller

Let us illustrate this with an example. Here I have a pendulum without actuating signal or controlled variable and I would like to make sure that my desired pendulum always faces gravity. In the other words, I have an encoder to measure the angle of the pendulum and this should be zero. This is what I set on my sensor.


<iframe width="560" height="315" src="https://www.youtube.com/embed/fshyjg3irzU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This is a pendulum without a controller which we also call an 'open loop'.

Since I swing my pendulum, my sensor read it as 30 degrees away from the desired angle. Therefore, my error is 0 minus 30 so I still need to move my pendulum to -30 degrees. The controller is the brain where it needs to tell my pendulum to how much speed the machine needs to move linearly.

![](./assets/images/invertedPendulum/pendulumExample.jpg)

# Not a Pendulum

## Proportional Controller
One way to do that is to use the error at the present moment to decide the speed it needs to travel. Here I set it to 0.1. This means if the error is -30 and multiply this by 0.1. I would get how much distance it needs to get the actuating signal. So this proportional controller seems not bad, just multiplying the present error by a constant. However, what you could see here is that when the pendulum is at the angle let's say 30 degrees and it reaches 0 (the desired input), the controller will shut off the motor traveling the linear movement. Then, the pendulum will continue to swing further.

<iframe width="560" height="315" src="https://www.youtube.com/embed/veXYX_hRY8E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In my observation, the pendulum gives us a steady error no matter how high I create the gain. The error won't go away, it just gets smaller. This error is called 'steady-state error'.

![](./assets/images/invertedPendulum/propotionalProblem.jpg)

## PID Controller

How can we get rid of this steady-state error? By using the past information, we would introduce what we call 'Integral'. When the error is in the steady-state, the non-zero value is integrated and the output will increase. However, we would again get the desired output but yet it's not ideal for this problem. We would get another problem which we call overshoot. Luckily, there is a solution for this, a 'derivative'. A derivative measures the rate of change. It means how fast is the error increases or decreases. Therefore, the derivative slows down when the pendulum reaches its goal.

![](./assets/images/invertedPendulum/derivative.jpg)

![](./assets/images/invertedPendulum/integral.jpg)


All in all, the PID controller uses its present, past, and prediction error to calculate the actuator signal. To do that, I would have to tune the constant of Kp, Ki, and Kd. 


<iframe width="560" height="315" src="https://www.youtube.com/embed/aapDCnNCaUM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Inverted Pendulum

<iframe width="560" height="315" src="https://www.youtube.com/embed/-E7s9KqwSA4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Useful Links
-   [Wikipedia Inverted Pendulum](https://en.wikipedia.org/wiki/Inverted_pendulum)

-   [Wikipedia PID](https://en.wikipedia.org/wiki/PID_controller)
