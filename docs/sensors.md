---
layout: page
title: Sensors
permalink: /Sensors
---


# Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

# Capacitive Touch Sensor
![](./assets/images/sensors/capacitive_sensor.gif)

[DFRobot_Capacitive_Touch_Sensor_SKU_DFR0030](https://wiki.dfrobot.com/DFRobot_Capacitive_Touch_Sensor_SKU_DFR0030)


## Circuit Diagram
{: .no_toc }
## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania 19/05/2020
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 200 # window size

def idle(parent,canvas):
    global filt,eps
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))


    if (char == '1'):
        x = 1
        text = 'button'
        canvas.itemconfigure("rect",fill="#b00000")
        canvas.itemconfigure("text",text="Touched")
    else:
        x = 0
        text = 'off'
        canvas.itemconfigure("rect",fill="#0000b0")
        canvas.itemconfigure("text",text="Untouched")


    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14140',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('hello.HC-SR501.py (q to exit)')
root.bind('q','exit')
canvas = Canvas(root, width=2*WINDOW, height=WINDOW, background='white')
canvas.create_text(.5*WINDOW,.5*WINDOW,text="read",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(WINDOW,0,2*WINDOW,WINDOW, tags='rect', fill='#b00000')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```

### Mircocontroller board
{: .no_toc }
```cpp
int ledPin = 13;             // Connect LED on pin 13, or use the onboard one
int KEY = 2;                 // Connect Touch sensor on Digital Pin 2

void setup(){
  pinMode(ledPin, OUTPUT);   // Set ledPin to output mode
  pinMode(KEY, INPUT);       //Set touch sensor pin to input mode
  Serial.begin(9600);
}

void loop(){
   if(digitalRead(KEY)==HIGH) {       //Read Touch sensor signal
        digitalWrite(ledPin, HIGH);   // if Touch sensor is HIGH, then turn on
        Serial.println(1);
   }
   else{
        digitalWrite(ledPin, LOW);    // if Touch sensor is LOW, then turn off the led
        Serial.println(0);
   }
   delay(100);
}
```






# Digital Vibration Sensor
![](./assets/images/sensors/digital_vibration.gif)

[DFRobot_Digital_Vibration_Sensor_V2_SKU_DFR0027](https://wiki.dfrobot.com/DFRobot_Digital_Vibration_Sensor_V2_SKU_DFR0027)

## Circuit Diagram
{: .no_toc }
## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 200 # window size

def idle(parent,canvas):
    global filt,eps
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))


    if (char == '1'):
        x = 1
        text = 'button'
        canvas.itemconfigure("rect",fill="#b00000")
        canvas.itemconfigure("text",text="Detected")
    else:
        x = 0
        text = 'off'
        canvas.itemconfigure("rect",fill="#0000b0")
        canvas.itemconfigure("text",text="Undetected")


    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('hello.HC-SR501.py (q to exit)')
root.bind('q','exit')
canvas = Canvas(root, width=2*WINDOW, height=WINDOW, background='white')
canvas.create_text(.5*WINDOW,.5*WINDOW,text="read",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(WINDOW,0,2*WINDOW,WINDOW, tags='rect', fill='#b00000')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```

### Microcontroller
{: .no_toc }
```cpp
#define SensorLED   13
//Connect the sensor to digital Pin 3 which is Interrupts 1.
#define SensorINPUT  3
unsigned char state = 0;

void setup() {
  pinMode(SensorLED, OUTPUT);
  pinMode(SensorINPUT, INPUT);
  //Trigger the blink function when the falling edge is detected
  attachInterrupt(1, blink, FALLING);
  Serial.begin(9600);
}

void loop() {
  if (state != 0) {
    state = 0;
    digitalWrite(SensorLED, HIGH);
    Serial.println(1);
  }else{
    digitalWrite(SensorLED, LOW);
    Serial.println(0);
  }
  delay(500);
}

//Interrupts function 
void blink() {
    state++;
}

```




# Digital Magnetic Sensor
![](./assets/images/sensors/digital_magnet.gif)

[Digital_magnetic_sensor_SKU__DFR0033](https://wiki.dfrobot.com/Digital_magnetic_sensor_SKU__DFR0033)

## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# Digital magnetic sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 200 # window size

def idle(parent,canvas):
    global filt,eps
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))


    if (char == '1'):
        x = 1
        text = 'button'
        canvas.itemconfigure("rect",fill="#b00000")
        canvas.itemconfigure("text",text="Detected")
    else:
        x = 0
        text = 'off'
        canvas.itemconfigure("rect",fill="#0000b0")
        canvas.itemconfigure("text",text="Undetected")


    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('hello.HC-SR501.py (q to exit)')
root.bind('q','exit')
canvas = Canvas(root, width=2*WINDOW, height=WINDOW, background='white')
canvas.create_text(.5*WINDOW,.5*WINDOW,text="read",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(WINDOW,0,2*WINDOW,WINDOW, tags='rect', fill='#b00000')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```

### Microcontroller
{: .no_toc }
```cpp
int ledPin = 13;                // choose the pin for the LED
int inputPin = 2;               // choose the input pin
int val = 0;                    // variable for reading the pin status
void setup() {
  pinMode(ledPin, OUTPUT);      // declare LED as output
  pinMode(inputPin, INPUT);     // declare pushbutton as input
  Serial.begin(9600);
}
void loop(){
  val = digitalRead(inputPin);  // read input value
  if (val == HIGH) {            // check if the input is HIGH
    digitalWrite(ledPin, HIGH);  // turn LED ON
    Serial.println(1);
  } else {
    digitalWrite(ledPin, LOW); // turn LED HIGH
    Serial.println(0);
  }
  delay(100);
}
```



# Digital Push Button

![](./assets/images/sensors/button.gif)

[DFRobot_Digital_Push_Button_SKU_DFR0029](https://wiki.dfrobot.com/DFRobot_Digital_Push_Button_SKU_DFR0029)

## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 200 # window size

def idle(parent,canvas):
    global filt,eps
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))


    if (char == '1'):
        x = 1
        text = 'button'
        canvas.itemconfigure("rect",fill="#b00000")
        canvas.itemconfigure("text",text="Pressed")
    else:
        x = 0
        text = 'off'
        canvas.itemconfigure("rect",fill="#0000b0")
        canvas.itemconfigure("text",text="Unpressed")


    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('hello.HC-SR501.py (q to exit)')
root.bind('q','exit')
canvas = Canvas(root, width=2*WINDOW, height=WINDOW, background='white')
canvas.create_text(.5*WINDOW,.5*WINDOW,text="read",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(WINDOW,0,2*WINDOW,WINDOW, tags='rect', fill='#b00000')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()

```

### Microcontroller
{: .no_toc }
```cpp
/*
  # Description:
  # When you push the digital button, the Led 13 on the board will turn off. Otherwise,the led turns on.
*/
int ledPin = 13;                // choose the pin for the LED
int inputPin = 2;               // Connect sensor to input pin 2


void setup() {
  pinMode(ledPin, OUTPUT);      // declare LED as output
  pinMode(inputPin, INPUT);     // declare pushbutton as input
  Serial.begin(9600);
}

void loop(){
  int val = digitalRead(inputPin);  // read input value
  if (val == HIGH) {            // check if the input is HIGH
    digitalWrite(ledPin, HIGH);  // turn LED ON
    Serial.println(1);
  } else {
    digitalWrite(ledPin, LOW); // turn LED OFF
    Serial.println(0);
  }
  delay(100);
}
```


# Digital Tilt Sensor


![](./assets/images/sensors/digital_tilt.gif)

[Digital_Tilt_Sensor_SKU_DFR0028](https://wiki.dfrobot.com/Digital_Tilt_Sensor_SKU_DFR0028)


## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 200 # window size

def idle(parent,canvas):
    global filt,eps
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))


    if (char == '1'):
        x = 1
        text = 'button'
        canvas.itemconfigure("rect",fill="#b00000")
        canvas.itemconfigure("text",text="Tilted")
    else:
        x = 0
        text = 'off'
        canvas.itemconfigure("rect",fill="#0000b0")
        canvas.itemconfigure("text",text="Leveled")


    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('hello.HC-SR501.py (q to exit)')
root.bind('q','exit')
canvas = Canvas(root, width=2*WINDOW, height=WINDOW, background='white')
canvas.create_text(.5*WINDOW,.5*WINDOW,text="read",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(WINDOW,0,2*WINDOW,WINDOW, tags='rect', fill='#b00000')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```

### Microcontroller
{: .no_toc }
```cpp
int ledPin = 13;                // Connect LED to pin 13
int switcher = 2;               // Connect Tilt sensor to Pin 2

void setup() {
  pinMode(ledPin, OUTPUT);      // Set digital pin 13 to output mode
  pinMode(switcher, INPUT);       // Set digital pin 2 to input mode
  Serial.begin(9600);
}

void loop() {
  if (digitalRead(switcher) == HIGH){ //Read sensor value
    digitalWrite(ledPin, LOW);   // Turn on LED when the sensor is tilted
    Serial.println(0);
  }else{
    digitalWrite(ledPin, HIGH);    // Turn off LED when the sensor is not triggered
    Serial.println(1);
  }
  delay(100);
}
```



# Digital Infrared Motion Sensor

![](./assets/images/sensors/digital_motion.gif)

[Digital_Infrared_motion_sensor__SKU_SEN0018_](https://wiki.dfrobot.com/Digital_Infrared_motion_sensor__SKU_SEN0018_)

## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 200 # window size

def idle(parent,canvas):
    global filt,eps
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))


    if (char == '1'):
        x = 1
        text = 'button'
        canvas.itemconfigure("rect",fill="#b00000")
        canvas.itemconfigure("text",text="Motion")
    else:
        x = 0
        text = 'off'
        canvas.itemconfigure("rect",fill="#0000b0")
        canvas.itemconfigure("text",text="Off")


    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('hello.HC-SR501.py (q to exit)')
root.bind('q','exit')
canvas = Canvas(root, width=2*WINDOW, height=WINDOW, background='white')
canvas.create_text(.5*WINDOW,.5*WINDOW,text="read",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(WINDOW,0,2*WINDOW,WINDOW, tags='rect', fill='#b00000')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```

### Microncontroller
{: .no_toc }
```cpp
const int buttonPin = 2;
const int ledPin =  13;
void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  Serial.begin(9600);
}
void loop()
{
   if (digitalRead(buttonPin) == HIGH)
   {
     digitalWrite(ledPin, HIGH);
     Serial.println(1);
   }
   else {
     digitalWrite(ledPin, LOW);
     Serial.println(0);
   }
   delay(100);
}
```



# Potentiometer
![](./assets/images/sensors/potentiometer.gif)

[https://wiki.dfrobot.com/Analog_Rotation_Sensor_V1__SKU__DFR0054_](https://wiki.dfrobot.com/Analog_Rotation_Sensor_V1__SKU__DFR0054_)

## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 600 # window size

def idle(parent,canvas):
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))
    
    canvas.itemconfigure("text",text="%s"%char)
    x = int(.2*WINDOW + (.9-.2)*WINDOW*int(char)/1024)
    canvas.coords('rect1',.2*WINDOW,.05*WINDOW,x,.2*WINDOW)
    canvas.coords('rect2',x,.05*WINDOW,.9*WINDOW,.2*WINDOW)

 
    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('Analog Sensor')
root.bind('q','exit')
canvas = Canvas(root, width=WINDOW, height=.25*WINDOW, background='white')
canvas.create_text(.1*WINDOW,.125*WINDOW,text="0",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(.2*WINDOW,.05*WINDOW,.3*WINDOW,.2*WINDOW, tags='rect1', fill='#b00000')
canvas.create_rectangle(.3*WINDOW,.05*WINDOW,.9*WINDOW,.2*WINDOW, tags='rect2', fill='#0000b0')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()


```

### Microcontroller
{: .no_toc }
```cpp
void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(100);
}
```


# Flame Sensor
![](./assets/images/sensors/analog_flame.gif)

[https://wiki.dfrobot.com/Flame_sensor_SKU__DFR0076](https://wiki.dfrobot.com/Flame_sensor_SKU__DFR0076)

## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 600 # window size

def idle(parent,canvas):
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))
    
    canvas.itemconfigure("text",text="%s"%char)
    x = int(.2*WINDOW + (.9-.2)*WINDOW*int(char)/1024)
    canvas.coords('rect1',.2*WINDOW,.05*WINDOW,x,.2*WINDOW)
    canvas.coords('rect2',x,.05*WINDOW,.9*WINDOW,.2*WINDOW)

 
    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('Analog Sensor')
root.bind('q','exit')
canvas = Canvas(root, width=WINDOW, height=.25*WINDOW, background='white')
canvas.create_text(.1*WINDOW,.125*WINDOW,text="0",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(.2*WINDOW,.05*WINDOW,.3*WINDOW,.2*WINDOW, tags='rect1', fill='#b00000')
canvas.create_rectangle(.3*WINDOW,.05*WINDOW,.9*WINDOW,.2*WINDOW, tags='rect2', fill='#0000b0')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()

```

### Microcontroller
{: .no_toc }
```cpp
void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(100);
}
```


# Steam Sensor

![](./assets/images/sensors/analog_steam.gif)

[Steam_Sensor__SKU_SEN0121_](https://wiki.dfrobot.com/Steam_Sensor__SKU_SEN0121_)


## Code
{: .no_toc }
### Microcontroller
{: .no_toc }
```cpp
/*
Connection:
VCC-5V
GND-GND
S-Analog pin 0
*/

void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(100);
}
```

### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 600 # window size

def idle(parent,canvas):
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))
    
    canvas.itemconfigure("text",text="%s"%char)
    x = int(.2*WINDOW + (.9-.2)*WINDOW*int(char)/150)
    canvas.coords('rect1',.2*WINDOW,.05*WINDOW,x,.2*WINDOW)
    canvas.coords('rect2',x,.05*WINDOW,.9*WINDOW,.2*WINDOW)

 
    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('Analog Sensor')
root.bind('q','exit')
canvas = Canvas(root, width=WINDOW, height=.25*WINDOW, background='white')
canvas.create_text(.1*WINDOW,.125*WINDOW,text="0",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(.2*WINDOW,.05*WINDOW,.3*WINDOW,.2*WINDOW, tags='rect1', fill='#b00000')
canvas.create_rectangle(.3*WINDOW,.05*WINDOW,.9*WINDOW,.2*WINDOW, tags='rect2', fill='#0000b0')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```



# Analog Grayscale Sensor

![](./assets/images/sensors/analog_grayscale.gif)

[DFRobot_Grayscale_Sensor__SKU_DFR0022_](https://wiki.dfrobot.com/DFRobot_Grayscale_Sensor__SKU_DFR0022_)

## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 600 # window size

def idle(parent,canvas):
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))
    
    canvas.itemconfigure("text",text="%s"%char)
    x = int(.2*WINDOW + (.9-.2)*WINDOW*int(char)/1024)
    canvas.coords('rect1',.2*WINDOW,.05*WINDOW,x,.2*WINDOW)
    canvas.coords('rect2',x,.05*WINDOW,.9*WINDOW,.2*WINDOW)

 
    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('Analog Sensor')
root.bind('q','exit')
canvas = Canvas(root, width=WINDOW, height=.5*WINDOW, background='white')
canvas.create_text(.1*WINDOW,.125*WINDOW,text="0",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(.2*WINDOW,.05*WINDOW,.3*WINDOW,.2*WINDOW, tags='rect1', fill='#b00000')
canvas.create_rectangle(.3*WINDOW,.05*WINDOW,.9*WINDOW,.2*WINDOW, tags='rect2', fill='#0000b0')
canvas.create_rectangle(0,0.25*WINDOW,0.5*WINDOW,0.45*WINDOW, tags='rect', fill='black')
canvas.create_rectangle(0.5*WINDOW,0.25*WINDOW,WINDOW,0.45*WINDOW, tags='rect', fill='white')

canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```

### Microcontroller
{: .no_toc }
```cpp
/*
Connection:
VCC-5V
GND-GND
S-Analog pin 0
*/

void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(100);
}
```



# Analog Sound Sensor

![](./assets/images/sensors/analog_sound.gif)

[Analog_Sound_Sensor_SKU__DFR0034](https://wiki.dfrobot.com/Analog_Sound_Sensor_SKU__DFR0034)

## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 600 # window size

def idle(parent,canvas):
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))
    
    canvas.itemconfigure("text",text="%s"%char)
    x = int(.2*WINDOW + (.9-.2)*WINDOW*int(char)/1024)
    canvas.coords('rect1',.2*WINDOW,.05*WINDOW,x,.2*WINDOW)
    canvas.coords('rect2',x,.05*WINDOW,.9*WINDOW,.2*WINDOW)

 
    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('Analog Sensor')
root.bind('q','exit')
canvas = Canvas(root, width=WINDOW, height=.25*WINDOW, background='white')
canvas.create_text(.1*WINDOW,.125*WINDOW,text="0",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(.2*WINDOW,.05*WINDOW,.3*WINDOW,.2*WINDOW, tags='rect1', fill='#b00000')
canvas.create_rectangle(.3*WINDOW,.05*WINDOW,.9*WINDOW,.2*WINDOW, tags='rect2', fill='#0000b0')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```

### Microcontroller
{: .no_toc }
```cpp
/*
Connection:
VCC-5V
GND-GND
S-Analog pin 0
*/

void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(100);
}
```


# Soil Moisture Sensor
![](./assets/images/sensors/moisture_sensor.gif)

[Capacitive_Soil_Moisture_Sensor_SKU_SEN0193](https://wiki.dfrobot.com/Capacitive_Soil_Moisture_Sensor_SKU_SEN0193)


## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 600 # window size

def idle(parent,canvas):
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))
    
    canvas.itemconfigure("text",text="%s"%char)
    x = int(.2*WINDOW + (.9-.2)*WINDOW*int(char)/1024)
    canvas.coords('rect1',.2*WINDOW,.05*WINDOW,x,.2*WINDOW)
    canvas.coords('rect2',x,.05*WINDOW,.9*WINDOW,.2*WINDOW)

 
    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('Analog Sensor')
root.bind('q','exit')
canvas = Canvas(root, width=WINDOW, height=.25*WINDOW, background='white')
canvas.create_text(.1*WINDOW,.125*WINDOW,text="0",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(.2*WINDOW,.05*WINDOW,.3*WINDOW,.2*WINDOW, tags='rect1', fill='#b00000')
canvas.create_rectangle(.3*WINDOW,.05*WINDOW,.9*WINDOW,.2*WINDOW, tags='rect2', fill='#0000b0')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```

### Microcontroller
{: .no_toc }
```cpp
/*
Connection:
VCC-5V
GND-GND
S-Analog pin 0
*/

void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(100);
}
```



# Analog Ambient Light Sensor

![](./assets/images/sensors/analog_light.gif)

[DFRobot_Ambient_Light_Sensor_SKU_DFR0026](https://wiki.dfrobot.com/DFRobot_Ambient_Light_Sensor_SKU_DFR0026)

## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 600 # window size

def idle(parent,canvas):
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))
    
    canvas.itemconfigure("text",text="%s"%char)
    x = int(.2*WINDOW + (.9-.2)*WINDOW*int(char)/1024)
    canvas.coords('rect1',.2*WINDOW,.05*WINDOW,x,.2*WINDOW)
    canvas.coords('rect2',x,.05*WINDOW,.9*WINDOW,.2*WINDOW)

 
    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('Analog Sensor')
root.bind('q','exit')
canvas = Canvas(root, width=WINDOW, height=.25*WINDOW, background='white')
canvas.create_text(.1*WINDOW,.125*WINDOW,text="0",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(.2*WINDOW,.05*WINDOW,.3*WINDOW,.2*WINDOW, tags='rect1', fill='#b00000')
canvas.create_rectangle(.3*WINDOW,.05*WINDOW,.9*WINDOW,.2*WINDOW, tags='rect2', fill='#0000b0')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```


### Microcontroller
{: .no_toc }
```cpp
/*
Connection:
VCC-5V
GND-GND
S-Analog pin 0
*/

void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(100);
}
```


<!--
# Analog Voltage Devider
[Analog_Voltage_Divider_SKU__DFR0051](https://wiki.dfrobot.com/Analog_Voltage_Divider_SKU__DFR0051)

## Code
{: .no_toc }
## Result
{: .no_toc }
-->
# Piezo Disk Vibration Sensor
![](./assets/images/sensors/piezo.gif)
https://wiki.dfrobot.com/Analog_Piezo_Disk_Vibration_Sensor__SKU_DFR0052_


## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 600 # window size

def idle(parent,canvas):
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))
    
    canvas.itemconfigure("text",text="%s"%char)
    x = int(.2*WINDOW + (.9-.2)*WINDOW*int(char)/1024)
    canvas.coords('rect1',.2*WINDOW,.05*WINDOW,x,.2*WINDOW)
    canvas.coords('rect2',x,.05*WINDOW,.9*WINDOW,.2*WINDOW)

 
    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('Analog Sensor')
root.bind('q','exit')
canvas = Canvas(root, width=WINDOW, height=.25*WINDOW, background='white')
canvas.create_text(.1*WINDOW,.125*WINDOW,text="0",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(.2*WINDOW,.05*WINDOW,.3*WINDOW,.2*WINDOW, tags='rect1', fill='#b00000')
canvas.create_rectangle(.3*WINDOW,.05*WINDOW,.9*WINDOW,.2*WINDOW, tags='rect2', fill='#0000b0')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()
```

### Microcontroller
{: .no_toc }

```cpp
/*
Connection:
VCC-5V
GND-GND
S-Analog pin 0
*/

void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  delay(100);
}
```




# Analog Carbon Monoxide Sensor (MQ7)

![](./assets/images/sensors/gas.gif)

[Carbon_Monoxide_Gas_Sensor_MQ7___SKU_SEN0132_](https://wiki.dfrobot.com/Carbon_Monoxide_Gas_Sensor_MQ7___SKU_SEN0132_)


## Code
{: .no_toc }
### Python
{: .no_toc }
```python
# -*- coding: utf-8 -*-
#
# hello.button.py
#
# button sensor detector hello-world
#
# Neil Gershenfeld 11/16/15
# (c) Massachusetts Institute of Technology 2015
#
# Modified: Marcello Tania
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose. Copyright is
# retained and must be preserved. The work is provided
# as is; no warranty is provided, and users accept all 
# liability.
#

from tkinter import *
import serial

WINDOW = 600 # window size

def idle(parent,canvas):
    #
    # idle routine
    #
    char = ser.readline()
    char = str(char).replace("b", "").replace("\\r", "").replace("\\n", "").replace("'", "")
    print("arduino:{}".format(char))
    
    canvas.itemconfigure("text",text="%s"%char)
    x = int(.2*WINDOW + (.9-.2)*WINDOW*int(char)/1024)
    canvas.coords('rect1',.2*WINDOW,.05*WINDOW,x,.2*WINDOW)
    canvas.coords('rect2',x,.05*WINDOW,.9*WINDOW,.2*WINDOW)

 
    canvas.update()
    parent.after_idle(idle,parent,canvas)


#
# open serial port
#

ser = serial.Serial('/dev/cu.usbserial-14440',9600)
ser.setDTR()
ser.flush()
#
# set up GUI
#
root = Tk()
root.title('Analog Sensor')
root.bind('q','exit')
canvas = Canvas(root, width=WINDOW, height=.25*WINDOW, background='white')
canvas.create_text(.1*WINDOW,.125*WINDOW,text="0",font=("Helvetica", 24),tags="text",fill="#0000b0")
canvas.create_rectangle(.2*WINDOW,.05*WINDOW,.3*WINDOW,.2*WINDOW, tags='rect1', fill='#b00000')
canvas.create_rectangle(.3*WINDOW,.05*WINDOW,.9*WINDOW,.2*WINDOW, tags='rect2', fill='#0000b0')
canvas.pack()
#
# start idle loop
#
root.after(100,idle,root,canvas)
root.mainloop()

```

### Microcontroller
{: .no_toc }
```cpp
/*
Connection:
VCC-5V
GND-GND
S-Analog pin 0
*/

void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue,DEC);//Print the value to serial port

  delay(100);
}
```



<!---
# LM35 Analog Linear Termperature Sensor
https://wiki.dfrobot.com/DFRobot_LM35_Linear_Temperature_Sensor__SKU_DFR0023_

## Circuit Diagram

## Code

## Result


# Sharp GP2Y0A21 Distance Sensor
https://wiki.dfrobot.com/Sharp_GP2Y0A21_Distance_Sensor__10-80cm___SKU_SEN0014_


# Joystick Module
https://wiki.dfrobot.com/Joystick_Module_For_Arduino_SKU_DFR0061


# Triple Axis Accelerometer MMA7361
https://wiki.dfrobot.com/Triple_Axis_Accelerometer_MMA7361_SKU_DFR0143
-->

# Useful links

- [DF Robot Wiki](https://wiki.dfrobot.com/)

# References
- [http://academy.cba.mit.edu/classes/input_devices/index.html](http://academy.cba.mit.edu/classes/input_devices/index.html)


